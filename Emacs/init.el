; start package.el with emacs
(require 'package)
; add MELPA to repository list
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
; initialize package.el
(package-initialize)

;; Window-system
; Background color:black
(set-face-background 'default "#000000")
; Character color: white
(set-face-foreground 'default "#ffffff")

;; Window
(add-to-list 'default-frame-alist '(alpha . (0.85 0.85)))

;; Do not show startup message
(setq inhibit-startup-message t)

;; highlight line
(global-hl-line-mode t)
(show-paren-mode t)
(setq show-paren-style 'mixed)
(transient-mark-mode t)

;; Dsable scroll-bar mode
(scroll-bar-mode -1)

;; Do not make backup file
(setq backup-inhibited t)

;; Eliminate beep sound
(setq visible-bell t)
(setq ring-bell-function 'ignore)

;; Make C-h back space
(global-set-key "\C-h" 'delete-backward-char)

 
;; auto-complete ----------------------------------------------------------------
(require 'auto-complete)
; do default config for auto-complete
(require 'auto-complete-config)
(ac-config-default)

;; yasnippet --------------------------------------------------------------------
(add-to-list 'load-path
             (expand-file-name "~/.emacs.d/elpa/yasnippet-20180621.50/yasnippet"))

(require 'yasnippet)
(setq yas-snippet-dirs
      '("~/.emacs.d/mySnippets" 
        "~/.emacs.d/elpa/yasnippet-snippets-20180714.1322/snippets"
        ))

(yas-global-mode 1)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (flymake-google-cpplint yasnippet-snippets auto-complete))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

; flymake-google-cpplint -------------------------------------------------------
; let's define a function for flymake initialization
(defun my:flymake-google-init () 
  (require 'flymake-google-cpplint)
  (custom-set-variables
   '(flymake-google-cpplint-command "/home/hiromi/intel/intelpython3/bin/cpplint"))
  (flymake-google-cpplint-load)
)
(add-hook 'c-mode-hook 'my:flymake-google-init)
(add-hook 'c++-mode-hook 'my:flymake-google-init)
; start google-c-style with emacs
(require 'google-c-style)
(add-hook 'c-mode-common-hook 'google-set-c-style)

;; Display error and warning messages in minibuffer.
(custom-set-variables
 '(help-at-pt-timer-delay 0.5)
 '(help-at-pt-display-when-idle '(flymake-overlay)))

