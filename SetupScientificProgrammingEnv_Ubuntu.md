# Set up Ubuntu 18.04 environment for scientific calculations

## If you use Virtual machine
### Windows (VMware)

1. Set up file sharing   
   ```$ sudo apt-get update```   
   ```$ sudo apt-get upgrade```   
   ```$ sudo apt-get install build-essential```   
2. Go to VMware "Player" -> Manage -> Reinstall VMware Tools   
   - Extract vmware-tools-distrib   
   - Run "sudo ./vmware-install.pl"   
   - Go to VMware "Player" -> Manage -> Virtual Machine Settings -> Enable file sharing

### macOS (Parallels)
1. When you install Ubuntu by using Parallels, uncheck express installation!
2. Change the display resolution   
   ```$ cd /etc/default/```   
   ```$ sudo vi grub```   
3. Modify as follows:   
		Original : ```GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"```   
		Change to: ```GRUB_CMDLINE_LINUX_DEFAULT="vga=0x03e5"```   
	```$ sudo update-grub```   
	```$ reboot```   

	- How to use vi

4. Set up file sharing   
   ```$ sudo apt-get update```   
   ```$ sudo apt-get upgrade```   
   ```$ sudo apt-get install build-essential```   
   ```$ sudo apt-get install linux-headers-$(uname -r)```   
5. Get “Parallels Tools   
   ```$ sudo ./install```   
   (If this does not work, reboot MacOS)   
6. If your keybord layout is not standard english(US)   
   - ``` $ sudo dpkg-reconfigure keyboard-configuration```   

## Set up Ubuntu

1. Inherit PATH in sudo
	- ```$ sudo visudo```
	- Comment out the line,   
		```Defaults   secure_path="..."```
	- Add,   
		```Defaults env_keep += "PATH"```

2. If you use different partitions (NTFS) for dropbox/google drive, you need to change the ownership as follows:
	- ```$id -u```   
	(This should give you the user id you have which you will insert into fstab.)   

    The uid is your User ID. The one you got from id -u.   
    The gid is you Group ID. Normally the same as id -u but you can check it with id -g.   
    The umask is like chown but reversed.   
	- Open fstab   
		```$ sudo /etc/fstab```
	- Search for the line that is mounting the ntfs partition.   
Original line:   
	```/dev/disk/by-uuid/528FA7511D7B4FEE /mnt/528FA7511D7B4FEE auto nosuid,nodev,nofail,x-gvfs-show 0 0```   
Modified line:   
	```/dev/disk/by-uuid/528FA7511D7B4FEE /mnt/528FA7511D7B4FEE auto nosuid,nodev,nofail,x-gvfs-show,uid=1000,gid=1000,umask=002 0 0```

## Install emacs via apt-get
1. ```$ sudo apt-get install emacs25```
2. Copy elisp settings (elisp, site-start.d, and init.el) to /home/(username)/.emacs.d
3. Set up emacs
   - My settings (https://gitlab.com/hiromiyasuda/scientificprogrammingenvironment/blob/master/Emacs/SetupEmacs.md)
   
## Install Intel parallel_studio (Fortran, C/C++ compilers, and MKL)
1. ```$ sudo apt-get install g++```
2. Download
3. ```$ sh install_GUI.sh```
4. Add the following lines in /home/.bashrc   
```
   source /home/hiromi/intel/bin/ifortvars.sh intel64
   source /home/hiromi/intel/bin/iccvars.sh intel64
   source /home/hiromi/intel/mkl/bin/mklvars.sh intel64 ilp64
   source /home/hiromi/intel/compilers_and_libraries_2017/linux/bin/compilervars.sh intel64
   export PATH=$PATH:/opt/intel/bin
```
5. Modify /etc/ld.so.conf.d/*.conf file ( I made hyubuntu.conf)   
	```$ sudo emacs '/etc/ld.so.conf.d/hyubuntu.conf'```   
	Add ```/home/hiromi/intel/compilers_and_libraries/linux/lib/intel64_lin```   
	Add ```/home/hiromi/intel/compilers_and_libraries/linux/mkl/lib/intel64_lin```   
	After modification: ```$ sudo ldconfig``` in the terminal

## Install "Intel Distribution for Python"
1. ```$ cd (directory)```
2. ```$ sh setup_intel_python.sh```
3. Add the following line in /home/.bashrc
   ```export PATH="/opt/intel/intelpython3/bin:$PATH"```
4. Install bison, flex, zlib, hdf5, and cmake by using conda
5. Create a new environment and Install numpy, scipy, matplotlib, spyder
	- ```$ conda create -n env_py36 python=3.6```
	- ```$ source activate env_py36```
	- ```$ conda search numpy```
	- ```$ conda install numpy -n env_py36```

6. Set up font family for matplotlib
	- ```$ sudo apt-get install msttcorefonts -qq```
	- ```$ rm ~/.cache/matplotlib -rf```

## Install OpenMPI
1. Change the owner of /opt/ (You should change "username" according to your system)
   - ```$ sudo chown username /opt/```
2. Download and extract
   - ```$ cd (directory)```
   - ```$ ./configure --prefix=/opt/openmpi CC=icc CXX=icpc F77=ifort FC=ifort CFLAGS=-m64 CXXFLAGS=-m64 FCFLAGS=-m64 FFLAGS=-m64```
   - ```$ make -j2 all```
   - ```$ sudo make all install```
3. Modify .bashrc by adding the following sentences
```
PATH=/opt/openmpi/bin:$PATH
LD_LIBRARY_PATH=/opt/openmpi/lib:$LD_LIBRARY_PATH
MANPATH=/opt/openmpi/share/man:$MANPATH
export PATH
export LD_LIBRARY_PATH
export MANPATH
```
4. Modify conf file
   -```$ sudo emacs '/etc/ld.so.conf.d/hyubuntu.conf'```
   - Add ```/opt/openmpi/lib```
   - After modification,   
		```$ sudo ldconfig``` in the terminal

## Install texlive
1. ```$ sudo apt-get install texlive-full```

## Scientific library for C/C++ and Fortran
1. Install GSL (GNU Scientific Library; https://www.gnu.org/software/gsl/)
	- Download
	- ```$ cd (directory)```
	- ```$ ./configure CC=icc FC=ifort```
	- ```$ make```
	- ```$ sudo make install```   
	(If something wrong, 'make uninstall' to uninstall everything)
2. Install FGSL (Fortran interface to GSL; https://www.lrz.de/services/software/mathematik/gsl/fortran/)
	- Download
	- ```$ cd (directory)```
	- ```$ gsl_CFLAGS=-I/usr/local/include gsl_LIBS=-L/usr/local/lib ./configure CC=icc FC=ifort```
	- ```$ make```
	- ```$ sudo make install```
	- Modify /etc/ld.so.conf.d/*.conf file ( I made hy-ubuntu.conf)   
		```$ sudo emacs '/etc/ld.so.conf.d/hyubuntu.conf'```   
		Add ```/usr/local/lib```   
		After modification,   
			```$ sudo ldconfig``` in the terminal
	- To compile a Fortran code;   
		```ifort -lmkl_rt -O3 -I/usr/local/include/fgsl -L/usr/local/lib -lgsl -lfgsl -lgslcblas -shared -fPIC ....```

## Install tensorflow
1. ```$ conda install scikit-learn```
2. ```$ conda install tensorflow```

## Install OpenCV
1. Preparation
   - ```$ sudo apt-get install cmake libeigen3-dev libgtk-3-dev qt5-default freeglut3-dev libvtk6-qt-dev libtbb-dev ffmpeg libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev libjpeg-dev libpng++-dev libtiff5-dev libopenexr-dev libwebp-dev libopenblas-dev liblapacke-dev libgflags-dev libgoogle-glog-dev```
   - Install ceres-solver
	   1. ```git clone https://ceres-solver.googlesource.com/ceres-solver```
	   2. ```cd ceres-solver```
	   3. ```mkdir build && cd build```
	   4. ```cmake ..```
	   5. ```make -j4```
	   6. ```make test```
	   7. ```sudo make install```
   - Download from 
```
https://opencv.org/releases.html
https://github.com/opencv/opencv_contrib/releases
```
   - Extract files in Download
   - After extraction,  
   ```cp -r opencv_contrib-3.4.3 opencv-3.4.3/contrib```
   - ```$ cd ~/opencv-3.4.3/```
   - ```$ mkdir build```
   - ```$ cd build```
2. cmake
```
cmake -D CMAKE_CXX_COMPILER=icpc -D CMAKE_C_COMPILER=icc \
    -D CMAKE_CXX_FLAGS=-std=gnu++14 \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D ENABLE_PRECOMPILED_HEADERS=OFF\
    -D OPENCV_EXTRA_MODULES_PATH=../contrib/modules \
    -D PYTHON_EXECUTABLE=$(which python)  \
    -D PYTHON3_EXECUTABLE=$(which python)  \
    -D BUILD_opencv_python2=OFF \
    -D BUILD_opencv_python3=ON \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D INSTALL_C_EXAMPLES=OFF \
    -D WITH_MATLAB=OFF \
    -D WITH_QT=ON \
    -D WITH_VTK=ON \
    -D WITH_OPENGL=OFF \
    -D BUILD_opencv_dnn=OFF \
    -D BUILD_TESTS=OFF \
    -D BUILD_PERF_TESTS=OFF \
    -D BUILD_opencv_ts=OFF \
    -D BUILD_EXAMPLES=OFF ..
```
3. ```$ make -j2```
4. ```$ sudo make install```
5. ```$ sudo ldconfig```
6. Sym-link our openCV (cv2.so) bidings into our environment (my case: env_py36)   
   - ```$ ln -s /usr/local/lib/python3.6/site-packages/cv2.cpython-36m-x86_64-linux-gnu.so /home/hiromi/intel/intelpython3/envs/env_py36/lib/python3.6/site-packages/cv2.so```

## Install Gmsh
1. Just download
2. Modify .bashrc by adding the following line as follows:   
   - ```export PATH="/home/hiromi/gmsh-4.0.1-Linux64/bin:$PATH"```

## Install Bullet Physics
1. Download and extract in home
2. If you use Ubuntu, Modify build_cmake_pybullet_double.sh as follows:   

Original section 
```
cmake -DBUILD_PYBULLET=ON -DBUILD_PYBULLET_NUMPY=ON -DUSE_DOUBLE_PRECISION=ON -DCMAKE_BUILD_TYPE=Release .. || exit 1
```

Modified version    
```
cmake -DBUILD_PYBULLET=ON -DBUILD_PYBULLET_NUMPY=OFF -DUSE_DOUBLE_PRECISION=ON -DCMAKE_BUILD_TYPE=Release \
-DPYTHON_INCLUDE_DIR=$(python -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())")  \
-DPYTHON_LIBRARY=$(python -c "import distutils.sysconfig as sysconfig; print(sysconfig.get_config_var('LIBDIR'))") .. || exit 1
```
3. ```$ ./build_cmake_pybullet_double.sh```
4. Modify the bashrc as follows:   
   - Add ```export PYTHONPATH=/home/hiromi/bullet3-master/build_cmake/examples/pybullet:$PYTHONPATH```






