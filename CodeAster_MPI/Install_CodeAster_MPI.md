# Install Code_Aster (MPI version) in Ubuntu 18.04
Version: 13.6

## Preparation 
1. Install Intel Distribution for Python
2. Install Intel parallel_studio_xe_2017_update"5" (Note: parallel_studio_xe_"2018" does not work for Code_aster)
3. Use conda to create environment for Python 2.7
```
conda create -n env_py27 python=2.7
conda install numpy bison flex zlib cmake
```
4. Install via apt
```
sudo apt-get install libx11-dev zlib1g-dev libmotif-dev checkinstall qt4-dev-tools
```
### For Salome_meca 2018  
You need temporaly change the following "libstdc++" files in   
```
/home/hiromi/salome_meca/V2018.0.1_public/prerequisites/debianForSalome/lib   
```
by adding underscore in front of the file name   
- (Before change)     -> (after change)
- libstdc++.so        ->  "_libstdc++.so"
- libstdc++.so.6      ->  "_libstdc++.so.6"
- libstdc++.so.6.0.20 ->  "_libstdc++.so.6.0.20"

## Install Salome-meca (Serial)
1. Download
2. $ export PYTHON=/home/hiromi/intel/intelpython3/envs/env_py27/bin/python
3. $ ./salome-meca-2018.0.1-LGPL-1.run
4. Modify .bashrc as follows:  
	Add "export PATH=/home/hiromi/salome_meca/appli_V2017.0.2:$PATH"

## Compile Code_Aster (Serial, use Intel MKL)
1. Download and extract aster (I extracted under home)
2. $ cd aster-full-src-13.4.0
3. Modify setup.cfg as follows:
   - Comment  
		PREFER_COMPILER = 'GNU'
	b) Add
		PREFER_COMPILER = 'Intel'

		PREFER_COMPILER_mumps = PREFER_COMPILER
		PREFER_COMPILER_metis = PREFER_COMPILER
	c) Uncomment and modified as follows:
		CC='icc'
		CXX='icpc'
		F90='ifort'
		LD=F90
		LDFLAGS='-nofor_main'
		F90FLAGS="-fpe0 -traceback"
	
	e) Add
		_install_mfront=False

4) Modify check_compilers.py as follows:
	a) Insert
		self.is_v12  = True
	before
		self.fcheck(self.is_v11, 'Intel Compilers version is 11.x')
      		self.fcheck(self.is_v12, 'Intel Compilers version >= 12.0')
	
5) Modify products.py as follows:
	a) Modify Line 775:
		'./waf configure --use-config=aster_full_config --disable-mfront'

6) Change the owner of /opt/ (You should change "username" according to your system)
	a) $ sudo chown username /opt/

7) $ export PYTHON=/opt/intel/intelpython2/bin/python2

8) $ /opt/intel/intelpython2/bin/python2 setup.py install



=========================================================================================
=     Compile Code_Aster (MPI, use Intel MKL)                                           =
=========================================================================================

1) Create a host file for MPI
	$ echo "$HOSTNAME cpu=$(cat /proc/cpuinfo | grep processor | wc -l)" > /opt/aster/etc/codeaster/mpi_hostfile

2) Install ParMETIS (ver 4.0.3, I downloaded on 5/29/2018)
	a) Download from http://glaros.dtc.umn.edu/gkhome/metis/parmetis/download
	b) Extract ParMETIS under opt/
	c) $ cd parmetis-4.0.3
	d) $ make config prefix=/opt/parmetis-4.0.3
	e) $ make install
	
3) Install scotch
	a) Extract scotch from the origianl Code_Aster file (/home/hiromi/aster-full-src-13.4.0/SRC) under opt/
	b) $ cd /opt/scotch-6.0.4/src
	c) Copy the revised Make.inc to src/
	e) $ make scotch esmumps
	g) $ make ptscotch ptesmumps 
(If the computer has only 2 cores, modify "scotch-6.0.4/src/check/Makefile" for number of cores)
	f) Check $ make check
	h) Check $ make ptcheck

4) Install Mumps
	a) Extract Mumps from the origianl Code_Aster file under opt/
	b) Change the directory name to "mumps-5.1.1-mpi"
	c) $ cd /opt/mumps-5.1.1-mpi
	d) Delete Makefile.inc.in and Copy the revised Makefile.inc to the same directory
	e) $ make all

5) Install PETSc (Use petsc-3.7.7, instead of the latest one!)
	a) Extract petsc from the original Code_Aster file under opt/
	b) $ cd 
	c) Modify config/BuildSystem/config/packages/MUMPS.py as follows:

===== Original section (Line 3-8) ====================================================
class Configure(config.package.Package):
  def __init__(self, framework):
    config.package.Package.__init__(self, framework)
    self.gitcommit        = 'MUMPS_5.0.2-p2.tar.gz'
    self.download         = ['git://https://bitbucket.org/petsc/pkg-mumps.git',
                             'http://ftp.mcs.anl.gov/pub/petsc/externalpackages/MUMPS_5.0.2-p2.tar.gz']
===== (end) Original section (Line 3-8) ====================================================

===== Modified like this ====================================================
class Configure(config.package.Package):
  def __init__(self, framework):
    config.package.Package.__init__(self, framework)
    self.gitcommit = 'v5.1.2-p1'
    self.download = ['git://https://bitbucket.org/petsc/pkg-mumps.git',
                             'https://bitbucket.org/petsc/pkg-mumps/get/'+self.gitcommit+'.tar.gz']
    self.downloaddirnames = ['petsc-pkg-mumps']
===== (end) Modified like this ====================================================


	c) $ ./configure --configModules=PETSc.Configure --optionsModule=config.compilerOptions --with-cc=mpicc --with-cxx=mpicxx --with-fc=mpifort --with-blas-lapack-lib=${MKLROOT}/lib/intel64/libmkl_rt.so --with-blacs-lib=${MKLROOT}/lib/intel64/libmkl_blacs_openmpi_lp64.a --with-blacs-include=${MKLROOT}/include --download-scalapack=yes --download-hypre=yes --download-ml=yes --with-debugging=0 COPTFLAGS=-O1 CXXOPTFLAGS=-O1 FOPTFLAGS=-O1 --with-x=0 --with-shared-libraries=0 --download-mumps=yes --with-ptscotch-dir=/opt/scotch-6.0.4 --with-metis-dir=/opt/aster/public/metis-5.1.0 --download-mumps-commit=v5.1.2-p1 --with-ssl=0

	d) $ make PETSC_DIR=/opt/petsc-3.7.7 PETSC_ARCH=arch-linux2-c-opt all
	e) $ make PETSC_DIR=/opt/petsc-3.7.7 PETSC_ARCH=arch-linux2-c-opt test
	f) $ make PETSC_DIR=/opt/petsc-3.7.7 PETSC_ARCH=arch-linux2-c-opt streams



6) Compile Code_Aster for MPI -----------------------------------------------------------
	a) Open /opt/aster/etc/codeaster/asrun
	b) Modify as (Maybe you don't have to modify)
		mpi_get_procid_cmd : echo $OMPI_COMM_WORLD_RANK
	and
		mpirun_cmd : /opt/openmpi/bin/mpirun -np %(mpi_nbcpu)s %(program)s
	c) Extract aster-13.4.0.tgz from the original file (I extracted under opt/ and rename as aster-13.4.0-mpi)
	d) $ cd aster-13.4.0-mpi
	e) Place "Ubuntu_intel.py" and "Ubuntu_intel_openmpi.py" in the same directory
	f) Modify waftools/mathematics.py as follows:

(Maybe you don't have to change...)

======== (Begin) Original (Line 107-110) =======================================
if self.get_define('HAVE_MPI'):
        totest.append('-mkl=parallel')
        scalapack = ['-lmkl_scalapack' + suffix or '_core', '-lmkl_intel' + suffix]   # ia32: mkl_scalapack_core
        blacs = ['-lmkl_intel_thread', '-lmkl_blacs_intelmpi' + suffix] + ['-lmkl_lapack95' + suffix]
======== (End) Original (Line 107-110) =======================================

to

======== (Begin) Modified (Line 107-110) =======================================
if self.get_define('HAVE_MPI'):
        totest.append('-mkl=parallel')
        scalapack = ['-lmkl_scalapack' + suffix or '_core', '-lmkl_intel' + suffix]   # ia32: mkl_scalapack_core
        blacs = ['-lmkl_intel_thread', '-lmkl_blacs_openmpi' + suffix] + ['-lmkl_lapack95' + suffix]
======== (End) Modified (Line 107-110) =======================================

	g) Change the name of the file
		original: /opt/scotch-6.0.4/include/metis.h
		modified: /opt/scotch-6.0.4/include/metis_temp.h
		and
		original: /opt/scotch-6.0.4/include/parmetis.h
		modified: /opt/scotch-6.0.4/include/parmetis_temp.h
	f) $ export ASTER_ROOT=/opt/aster
	g) $ ./waf configure --use-config-dir=$ASTER_ROOT/13.4/share/aster --use-config=Ubuntu_intel_openmpi --prefix=$ASTER_ROOT/PAR13.4PE
	h) $ ./waf build -p
	i) $ ./waf install
	j) Open /opt/aster/etc/codeaster/aster and add the follows sentence
		vers : PAR13.4:/opt/aster/PAR13.4PE/share/aster 

7) Modify ./bashrc as follows:
	a) Add 
		# Salome-meca
		export PATH=/home/hiromi/salome_meca/appli_V2017.0.2:$PATH
		export ASTER_ROOT=~/aster
		export PATH=/opt/aster/bin:/opt/aster/outils:$PATH

8) When you use the MPI version of the Code_Aster
	a) Use Scotch in AFFE_MODELE section as follows:

modelCOQ = AFFE_MODELE(
    AFFE=_F(MODELISATION=('COQUE_3D', ), PHENOMENE='MECANIQUE', TOUT='OUI'),
    DISTRIBUTION=_F(PARTITIONNEUR='SCOTCH',
    METHODE='SOUS_DOMAINE',),
    MAILLAGE=meshMOD
)






